package com.kcpit.cmdclass;

import lombok.Data;

@Data
public class ContactDtlsCMD {
	private Integer conatctId;
	private String contactName;
	private String contactEmail;
	private Long contactNum;
}
