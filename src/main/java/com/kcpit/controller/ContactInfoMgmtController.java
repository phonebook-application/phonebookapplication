package com.kcpit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kcpit.cmdclass.ContactDtlsCMD;
import com.kcpit.service.ContactService;

@Controller
public class ContactInfoMgmtController {
	@Autowired
	private ContactService service;
	private static final String REGISTER = "contactinfo";


	@GetMapping(value = { "/", "/loadFrom.py" })
	public String loadFrom(@ModelAttribute("contact") ContactDtlsCMD c) {
		return REGISTER;
	}

	@PostMapping(value = { "/loadFrom.py" })
	public String handleSumitBtm(@ModelAttribute("contact") ContactDtlsCMD c, Model model) {
		boolean flag = service.saveContact(c);
		String msg = null;
		if (flag) {
			msg = "Contact Is Saved Successfully";
		} else {
			msg = "Contact Is  not Saved ";
		}
		model.addAttribute("msg", msg);
		return REGISTER;
	}

	@GetMapping(value = "/allconatct")
	public String getAllConatct(Model model) {
		List<ContactDtlsCMD> allContact = service.getAllContacts();
		model.addAttribute("contactList", allContact);
		return "showcontacts";
	}

	@GetMapping(path = "/updatecontact")
	public String updateContact(@RequestParam("cid") Integer conatctID, Model model) {
		ContactDtlsCMD contactById = service.getContactById(conatctID);
		model.addAttribute("contact", contactById);
		return REGISTER;
	}

	@GetMapping(value = { "/deletecontact" })
	public String deleteContact(@RequestParam("cid") Integer contactID, Model model) {
		String msg = null;
		boolean flag = service.contactDeleteById(contactID);
		if (flag) {
			msg = "successfully deleted";
		} else {
			msg = " contact not deleted ";
		}
		model.addAttribute("msg", msg);
		List<ContactDtlsCMD> allContact = service.getAllContacts();
		model.addAttribute("contactList", allContact);
		return "showcontacts";

	}
}
