package com.kcpit.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kcpit.entities.ContactDtlsEntity;

public interface ContactRepository extends JpaRepository<ContactDtlsEntity, Serializable> {

}
