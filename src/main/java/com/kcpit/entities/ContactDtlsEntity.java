package com.kcpit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
@Table(name = "contact_details")
public class ContactDtlsEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "contact_id")
	@SequenceGenerator(name = "cid_gen", sequenceName = "CONTACT_ID_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "cid_gen", strategy = GenerationType.SEQUENCE)
	private Integer contactId;
	@Column(name = "contact_name")
	private String contactName;
	@Column(name = "contact_email")
	private String contactEmail;
	@Column(name = "contact_num")
	private Long contactNum;
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "create_date", updatable = false)
	private Date createUserDate;
	@UpdateTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "update_date", insertable = false)
	private Date updateUserDate;
}
