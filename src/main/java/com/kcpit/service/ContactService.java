package com.kcpit.service;

import java.util.List;

import com.kcpit.cmdclass.ContactDtlsCMD;

public interface ContactService {
	boolean saveContact(ContactDtlsCMD conCmd);

	List<ContactDtlsCMD> getAllContacts();

	ContactDtlsCMD getContactById(Integer conId);

	boolean contactDeleteById(Integer conId);
}
