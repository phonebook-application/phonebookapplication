package com.kcpit.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kcpit.cmdclass.ContactDtlsCMD;
import com.kcpit.entities.ContactDtlsEntity;
import com.kcpit.repositories.ContactRepository;

@Service
public class ContactServiceImpl implements ContactService {
	@Autowired
	private ContactRepository repo;

	@Override
	public boolean saveContact(ContactDtlsCMD conCmd) {
		ContactDtlsEntity entity = null;
		entity = new ContactDtlsEntity();
		entity.setContactId(conCmd.getConatctId());
		entity.setContactName(conCmd.getContactName());
		entity.setContactNum(conCmd.getContactNum());
		entity.setContactEmail(conCmd.getContactEmail());
		repo.save(entity);
		return true;

	}

	@Override
	public List<ContactDtlsCMD> getAllContacts() {
		List<ContactDtlsEntity> conList = null;
		conList = repo.findAll();
		return conList.stream().map(entity -> {
			ContactDtlsCMD cmd = new ContactDtlsCMD();
			cmd.setConatctId(entity.getContactId());
			cmd.setContactName(entity.getContactName());
			cmd.setContactEmail(entity.getContactEmail());
			cmd.setContactNum(entity.getContactNum());
			return cmd;
		}).collect(Collectors.toList());
	}

	@Override
	public ContactDtlsCMD getContactById(Integer conId) {
		Optional<ContactDtlsEntity> contactDetail = repo.findById(conId);
		ContactDtlsEntity entity = null;
		ContactDtlsCMD cmd = null;
		if (contactDetail.isPresent()) {
			entity = contactDetail.get();
			cmd = new ContactDtlsCMD();
			cmd.setConatctId(entity.getContactId());
			cmd.setContactName(entity.getContactName());
			cmd.setContactEmail(entity.getContactEmail());
			cmd.setContactNum((entity.getContactNum()));
			return cmd;
		} else {
			return null;
		}

	}

	@Override
	public boolean contactDeleteById(Integer conId) {
		repo.deleteById(conId);
		return true;
	}
}
