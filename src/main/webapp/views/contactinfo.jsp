<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Patient RegisterPage</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<!--validatation logic  -->
	<div class="container">
		<form:form modelAttribute="contact" action="loadFrom.py" method="post">
			<div class="card text-center text-white bg-success mb-3">
				<div class="card-body ">
					<h2 class="card-title">Save Contact</h2>
					<!-- Book Name -->
					<div class="form-group row">
						<form:hidden path="conatctId" />
						<label class="col-sm-2 ">Contact Name::</label>
						<div class="col-sm-6">
							<form:input path="contactName" placeholder=" Enter Name" />
						</div>
					</div>
					<!-- Author-->
					<div class="form-group row">
						<label class="col-sm-2 ">Contact Email::</label>
						<div class="col-sm-6">
							<form:input path="contactEmail" placeholder="Enter Email" />
						</div>
					</div>

					<!-- Price -->
					<div class="form-group row">
						<label class="col-sm-2 ">Contact Number::</label>
						<div class="col-sm-6">
							<form:input path="ContactNum" placeholder="Enter Number" />
						</div>
					</div>

					<div>
						<button type="submit" class="btn btn-outline-danger">Insert
							Contact</button>
					</div>
					<br>
					<br>
					<div>
						<a href="allconatct" class="btn btn-danger btn-lg">Get All
							Contacts</a>
					</div>
				</div>
				<div>
					<h3>${msg }</h3>
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>