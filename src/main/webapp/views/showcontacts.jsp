<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="p"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Contact Details</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">

<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<script>
	$(document).ready(function() {
		$('.table').DataTable();
	});
	function deleteConfirm() {
		confirm("Tu delte kale gandi dabu");
	}
</script>
</head>
<body>
	<div class="container">
		<div>
			<a href="loadFrom.py" class="btn btn-success btn-sm">+ Add New
				Record</a>
		</div>
		<div class="card-header text-center" id="card-header2">
			<h1>All Book Details</h1>
		</div>
		<p:choose>
			<p:when test="${!empty contactList }">

				<table class="card-table table  table-image " id="contactTable">
					<caption>New York City Marathon Results 2013</caption>
					<thead class="text-center">
						<tr>
							<th scope="col">S.No</th>
							<th scope="col">User Name</th>
							<th scope="col">User Email</th>
							<th scope="col">Contact Number</th>
							<th colspan="2" scope="col">Actions</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<p:forEach var="contact" items="${contactList}" varStatus="index">

							<tr>
								<td><p:out value="${index.count}" /></td>
								<td><p:out value="${contact.contactName }" /></td>
								<td><p:out value="${contact.contactEmail}" /></td>
								<td><p:out value="${contact.contactNum}" /></td>
								<td><a href="updatecontact?cid=${contact.conatctId }"
									class="btn btn-primary">Update</a><a
									href="deletecontact?cid=${contact.conatctId }"
									class="btn btn-danger" onclick="deleteConfirm()">Delete</a></td>

							</tr>

						</p:forEach>
					</tbody>
				</table>
			</p:when>
			<p:otherwise>
	no student found
	</p:otherwise>
		</p:choose>
	</div>
	<div>
		<h2>${msg}</h2>
	</div>
</body>

</html>